import { Component, OnInit, ViewChild } from "@angular/core";
import { AuthService } from "../../core/service/auth.service";
import { ChartComponent } from "ng-apexcharts";
@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.sass"],
})
export class DashboardComponent implements OnInit {
  @ViewChild("chart") chart: ChartComponent;
  constructor(private serv: AuthService) {}
  riskGroups = [];
  factorsStatistic: any;
  sexStatistic: any;
  isloading = true;
  barChartOptions: any;
  barChart2Options: any;
  areaChartOptions: any;


  toggle(task, nav: any) {
    task.done = !task.done;
  }
  // TODO end

  ngOnInit() {
    this.serv.getDoctorDashboard().subscribe(data => {
      this.isloading = false;
      this.riskGroups = data['riskGroups'];
      this.factorsStatistic = data['factorsStatistic'];
      this.sexStatistic = data['sexStatistic'];
      let ar = [];
      this.factorsStatistic.categories.forEach(element => {
        ar.push(element.split("|"));
      });
      this.factorsStatistic.categories = ar;
      ar = [];
      this.sexStatistic.categories.forEach(element => {
        ar.push(element.split("|"));
      });
      this.sexStatistic.categories = ar;
      this.chart1();
      this.chart2();
      this.chart10();
    });

    
  }
  private chart10() {
    this.areaChartOptions = {
      series: [
        {
          name: "Первичный приём",
          data: [31, 40, 28, 51, 42],
        },
        {
          name: "Повторный приём",
          data: [11, 32, 45, 32, 34],
        },
      ],
      chart: {
        height: 350,
        type: "area",
        toolbar: {
          show: false,
        },
        foreColor: "#9aa0ac",
      },
      colors: ["#7D4988", "#66BB6A"],
      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: "smooth",
      },
      xaxis: {
        categories: [
          "Июль 2020",
          "Август 2020",
          "Сентябрь 2020",
          "Октябрь 2020",
          "Ноябрь 2020"
        ],
      },
      legend: {
        show: true,
        position: "top",
        horizontalAlign: "center",
        offsetX: 0,
        offsetY: 0,
      },

      tooltip: {
        theme: "dark",
        marker: {
          show: true,
        },
        x: {
          format: "dd/MM/yy HH:mm",
        },
      },
    };
  }
  private chart1() {
    this.barChartOptions = {
      series: this.factorsStatistic.data,
      chart: {
        type: "bar",
        height: 350,
        foreColor: '#9aa0ac'
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: "55%",
          endingShape: "rounded"
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        show: true,
        width: 2,
        colors: ["transparent"]
      },
      xaxis: {
        categories: this.factorsStatistic.categories,
        labels: {
          rotate: 0,
          style: {
            colors: '#9aa0ac',
          }
        }
      },
      yaxis: {
        title: {
          text: "Процент влияния"
        },
      },
      fill: {
        opacity: 1
      },
      tooltip: {
        theme: "dark",
        marker: {
          show: true,
        },
        x: {
          show: true,
        },
      },
    };
  }
  private chart2() {
    this.barChart2Options = {
      series: this.sexStatistic.data,
      chart: {
        type: "bar",
        height: 350,
        foreColor: '#9aa0ac'
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: "55%",
          endingShape: "rounded"
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        show: true,
        width: 2,
        colors: ["transparent"]
      },
      xaxis: {
        categories: this.sexStatistic.categories,
        labels: {
          style: {
            colors: '#9aa0ac',
          }
        }
      },
      yaxis: {
        title: {
          text: "Процент влияния"
        },
      },
      fill: {
        opacity: 1
      },
      tooltip: {
        theme: "dark",
        marker: {
          show: true,
        },
        x: {
          show: true,
        },
      },
    };
  }
}
