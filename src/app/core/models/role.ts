export enum Role {
  All = "Все",
  Admin = "Главный врач",
  Doctor = "Доктор",
  Patient = "Пациент",
}
